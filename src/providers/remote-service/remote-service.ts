import { Injectable } from '@angular/core';
import { Http , Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable'

/*
  Generated class for the RemoteServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RemoteServiceProvider {
  private url = "http://localhost:8080/api?sort=-id";
  constructor(public http: Http) {
    console.log('Hello RemoteServiceProvider Provider');
  }

  getMessage(){
    return this.http.get(this.url)
    .do(this.logResponse)
    .map(this.extractData)
    .catch(this.catchError);

  }

  private logResponse(res: Response) {
    console.log(res);
  }

  private extractData(res: Response){
    return res.json();
  }

  private catchError(error: Response | any){
    console.log(error);
    return Observable.throw(error.json().error || "Server Error");
  }

}
