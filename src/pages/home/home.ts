import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RemoteServiceProvider } from '../../providers/remote-service/remote-service'
import { AddTodoPage } from '../add-todo/add-todo';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  todoList = [];
  constructor(public navCtrl: NavController, private messageService : RemoteServiceProvider) {
    this.getMessages();
  }
  
  getMessages() {
    // this.messageService.getMessage().subscribe(data => console.log(data));
    this.messageService.getMessage().subscribe(data => this.todoList = data);
  }

  navigateToAddTodoPage() {
    this.navCtrl.push(AddTodoPage); 
  }
}
