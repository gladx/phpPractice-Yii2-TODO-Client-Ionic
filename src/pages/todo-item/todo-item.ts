import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { TodoItem } from '../../models/todo-item.interface';

@IonicPage()
@Component({
  selector: 'page-todo-item',
  templateUrl: 'todo-item.html',
})
export class TodoItemPage {

  public todoItem: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.todoItem = this.navParams.get('todoItem');
    console.log(this.todoItem );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoItemPage');
  }

}
