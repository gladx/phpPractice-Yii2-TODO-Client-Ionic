import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http , Headers, RequestOptions} from '@angular/http';
import { TodoItem } from '../../models/todo-item.interface';


@IonicPage()
@Component({
  selector: 'page-add-todo',
  templateUrl: 'add-todo.html',
})
export class AddTodoPage {
  // Creating new Object 
  todoItem = {} as TodoItem;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  addTodoItem(todoItem: TodoItem) {
    let headerOptions: any = { 'Content-Type': 'application/x-www-form-urlencoded' };
    let headers = new Headers(headerOptions);
      console.log(todoItem);
    let todoStr = "title="+todoItem.title+"&detail="+todoItem.detail+"&color="+todoItem.color;
      this.http.post("http://localhost:8080/api/create", todoStr, new RequestOptions({ headers: headers })).subscribe(data => {
        console.log(JSON.stringify(data.json()));
    }, error => {
        console.log(JSON.stringify(error.json()));
    });

    
      this.todoItem = {} as TodoItem;
      this.navCtrl.pop();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTodoPage');
  }

}
