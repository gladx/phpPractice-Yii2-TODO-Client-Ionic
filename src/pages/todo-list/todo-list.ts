import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';

import { RemoteServiceProvider } from '../../providers/remote-service/remote-service'
import { AddTodoPage } from '../add-todo/add-todo';
import { TodoItemPage } from '../todo-item/todo-item';
// import { TodoItem } from '../../models/todo-item.interface';


@Component({
  selector: 'page-todo-list',
  templateUrl: 'todo-list.html',
})
export class TodoListPage {

  todoList = [];
  constructor(public navCtrl: NavController, private messageService : RemoteServiceProvider) {
    this.getMessages();
  }
  
  getMessages() {
    // this.messageService.getMessage().subscribe(data => console.log(data));
    this.messageService.getMessage().subscribe(data => this.todoList = data);
  }

  navigateToAddTodoPage() {
    this.navCtrl.push(AddTodoPage); 
  }

  navigateToItemPage(todoItem: any) {
    this.navCtrl.push(TodoItemPage, {todoItem: todoItem}); 
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoListPage');
  }

}
