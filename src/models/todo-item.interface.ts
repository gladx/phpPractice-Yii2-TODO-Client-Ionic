export interface TodoItem {
    id: string,
    title: string,
    detail: string,
    color: string
}